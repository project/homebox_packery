
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Credits


INTRODUCTION
------------

This module is base on the homebox module that allows site administrators
to create dashboards for their users, using blocks as widgets.

But i wanted to use a different JavaScript layout library to create
a more flexible layout, like Packery Layout Library.

Packery layouts can be intelligently ordered or organically wild.
Elements can be stamped in place, fit in an ideal spot, or dragged around.
With this flexibility, the user experience if for me more interactive and fun !

This module add some settings options to the homebox settings page
to active Packery and set all the available options of Packery.


REQUIREMENTS
------------

1. Homebox (http://www.drupal.org/node/380866)
2. Libraries (http://www.drupal.org/node/465844)
3. Jquery update (https://www.drupal.org/node/139405)


INSTALLATION
------------

1. Install as usual, see http://drupal.org/node/895232 for further information.
   Note that installing external libraries is separate from installing this
   module and should happen in the sites/all/libraries directory. See
   http://drupal.org/node/1440066 for more information.

2. Download the Packery layout library from http://packery.metafizzy.co/

3. Place packery.pkgd.min.js in the library directory
   To have : sites/all/libraries/packery/packery.pkgd.min.js


CREDITS
-------

Current maintainers:
* Tom Blanchard (tomefa - tomefa@gmail.com) - http://www.drupal.org/user/1116410

This project has been sponsored by:
* YOGARIK
  As experts in Open Source web technologies, we provide our expertise
  to customers interested in implementing safe tools tailored
  to assist their company achieve its strategic goals.
  Visit http://www.yogarik.com or http://www.drupal.org/node/1606762
  for more information.
